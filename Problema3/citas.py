class Citas :
    def __init__(self, pnombre,apellido,medico,clinica):
        self.nombre = pnombre
        self.apellido = apellido
        self.medico = medico
        self.clinica = clinica
    def __str__(self):
        return f"Nombre = {self.nombre}, {self.apellido} Medico: {self.medico} Clinica: {self.clinica}"
    def getnombre(self):
        return self.nombre
    def getapellido(self):
        return self.apellido
    def setNombre(self, pNombre):
        self.nombre = pNombre
    def setApellido(self, pApellido):
        self.apellido = pApellido
    def setMedico(self, pMedico):
        self.medico = pMedico
    def setClinica(self, pClinica):
        self.clinica = pClinica




def agregar():
    nombre = input("Ingrese el Nombre: ")
    apellido = input("Ingrese el Apellido: ")
    medico = input("Ingrese el Nombre del Medico: ")
    clinica = input("Ingrese el Clinica: ")
    citaNueva = Citas (nombre, apellido, medico, clinica)
    listaCitas.append(citaNueva)

def informar():
    print(" ")
    print("----Informe de Citas----")
    for indice in range(0, len(listaCitas)):
        print(f"{indice +1} - {listaCitas[indice]}")

def borrar():
    informar()
    indice = int(input("Ingrese el numero de cita que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaCitas[indice -1].getapellido()} {listaCitas[indice-1].getnombre()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaCitas.remove(listaCitas[indice -1])

def modificar():
    informar()
    indice = int(input("Ingrese el numero de cita que desea modificar: "))
    nombre = input("Ingrese en nuevo nombre: ")
    listaCitas[indice - 1].setNombre(nombre)
    apellido = input("Ingrese en nuevo Apellido: ")
    listaCitas[indice - 1].setApellido(apellido)
    medico = input("Ingrese en nuevo Medico: ")
    listaCitas[indice - 1].setMedico(medico)
    clinica = input("Ingrese la nueva Clinica: ")
    listaCitas[indice - 1].setClinica(clinica)

def buscar():
    buscar = input("Nombre del Paciente o Clinica: ")



listaCitas = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Citas--------")
    print ("A - Agregar Cita")
    print ("M - Modificar Cita")
    print ("L - Listar las Citas")
    print ("B - Borrar una cita")
    print ("C - Busqueda de una cita una cita")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'a'):
        agregar()
    elif(opcion == 'l'):
        informar()
    elif(opcion == 'b'):
        borrar()
    elif(opcion == 'm'):
        modificar()
    elif(opcion == 'c'):
        buscar()
    else :
            print("Opcion Incorrecta")
