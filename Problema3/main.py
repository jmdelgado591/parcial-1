import os

def menu():
	"""
	Función que limpia la pantalla y muestra nuevamente el menu
	"""
	os.system('cls')
	print ("======Menu Principal======")
	print ("\t1 - Clínicas")
	print ("\t2 - Médicos")
	print ("\t3 - Pacientes")
	print ("\t4 - Citas")
	print ("\t9 - Salir")


while True:
	# Mostramos el menu
	menu()

	# solicituamos una opción al usuario
	opcionMenu = input("Inserta un numero >> ")

	if opcionMenu=="1":
		os.system('python clinicas.py')
	elif opcionMenu=="2":
		os.system('python medicos.py')
	elif opcionMenu=="3":
		os.system('python pacientes.py')
	elif opcionMenu=="4":
		os.system('python citas.py')
	elif opcionMenu=="9":
		break
	else:
		print ("")
		input("No has pulsado ninguna opción correcta...\npulsa una tecla para continuar"
