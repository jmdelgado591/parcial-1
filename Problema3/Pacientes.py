class Paciente :
    def __init__(self, pnombre,apellido,medico,clinica):
        self.nombre = pnombre
        self.apellido = apellido
        self.medico = medico
        self.clinica = clinica
    def __str__(self):
        return f"Nombre = {self.nombre}, {self.apellido} Medico: {self.medico} Clinica: {self.clinica}"
    def getnombre(self):
        return self.nombre
    def getapellido(self):
        return self.apellido
    def setNombre(self, pNombre):
        self.nombre = pNombre
    def setApellido(self, pApellido):
        self.apellido = pApellido
    def setMedico(self, pMedico):
        self.medico = pMedico
    def setClinica(self, pClinica):
        self.clinica = pClinica




def agregar():
    nombre = input("Ingrese el Nombre: ")
    apellido = input("Ingrese el Apellido: ")
    medico = input("Ingrese el Nombre del Medico: ")
    clinica = input("Ingrese el Clinica: ")
    pacienteNuevo = Paciente (nombre, apellido, medico, clinica)
    listaPacientes.append(pacienteNuevo)

def informar():
    print(" ")
    print("----Informe de Pacientes----")
    for indice in range(0, len(listaPacientes)):
        print(f"{indice +1} - {listaPacientes[indice]}")

def borrar():
    informar()
    indice = int(input("Ingrese el numero de cita que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaPacientes[indice -1].getapellido()} {listaPacientes[indice-1].getnombre()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaPacientes.remove(listaPacientes[indice -1])

def modificar():
    informar()
    indice = int(input("Ingrese el numero del Paciente que desea modificar: "))
    nombre = input("Ingrese en nuevo nombre: ")
    listaPacientes[indice - 1].setNombre(nombre)
    apellido = input("Ingrese en nuevo Apellido: ")
    listaPacientes[indice - 1].setApellido(apellido)
    medico = input("Ingrese en nuevo Medico: ")
    listaPacientes[indice - 1].setMedico(medico)
    clinica = input("Ingrese la nueva Clinica: ")
    listaPacientes[indice - 1].setClinica(clinica)

listaPacientes = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Pacientes--------")
    print ("A - Agregar un Paciente")
    print ("M - Modificar un Paciente")
    print ("L - Listar los Pacientes")
    print ("B - Borrar una Paciente")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'a'):
        agregar()
    elif(opcion == 'l'):
        informar()
    elif(opcion == 'b'):
        borrar()
    elif(opcion == 'm'):
        modificar()
    else :
            print("Opcion Incorrecta")
