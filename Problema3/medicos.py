class Medicos :
    def __init__(self, pnombre,apellido,clinica):
        self.nombre = pnombre
        self.apellido = apellido
        self.clinica = clinica
    def __str__(self):
        return f"Nombre = {self.nombre}, {self.apellido} Clinica: {self.clinica}"
    def getnombre(self):
        return self.nombre
    def getapellido(self):
        return self.apellido
    def setNombre(self, pNombre):
        self.nombre = pNombre
    def setApellido(self, pApellido):
        self.apellido = pApellido
    def setClinica(self, pClinica):
        self.clinica = pClinica




def agregar():
    nombre = input("Ingrese el Nombre: ")
    apellido = input("Ingrese el Apellido: ")
    clinica = input("Ingrese la Clinica: ")
    medicoNuevo = Medicos (nombre, apellido, clinica)
    listaMedicos.append(medicoNuevo)

def informar():
    print(" ")
    print("----Informe de Medicos----")
    for indice in range(0, len(listaMedicos)):
        print(f"{indice +1} - {listaMedicos[indice]}")

def borrar():
    informar()
    indice = int(input("Ingrese el numero del Medico que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaMedicos[indice -1].getapellido()} {listaMedicos[indice-1].getnombre()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaMedicos.remove(listaMedicos[indice -1])

def modificar():
    informar()
    indice = int(input("Ingrese el numero del Medico que desea modificar: "))
    nombre = input("Ingrese en nuevo nombre: ")
    listaMedicos[indice - 1].setNombre(nombre)
    apellido = input("Ingrese en nuevo Apellido: ")
    listaMedicos[indice - 1].setApellido(apellido)
    clinica = input("Ingrese la nueva Clinica: ")
    listaMedicos[indice - 1].setClinica(clinica)

listaMedicos = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Medicos--------")
    print ("A - Agregar un Medico")
    print ("M - Modificar un Medico")
    print ("L - Listar los Medicos")
    print ("B - Borrar una Medico")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'a'):
        agregar()
    elif(opcion == 'l'):
        informar()
    elif(opcion == 'b'):
        borrar()
    elif(opcion == 'm'):
        modificar()
    else :
            print("Opcion Incorrecta")
