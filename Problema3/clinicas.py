class Clinica :
    def __init__(self, pnombre,direccion,):
        self.nombre = pnombre
        self.direccion = direccion

    def __str__(self):
        return f"Nombre = {self.nombre}, Direccion: {self.direccion}"
    def getnombre(self):
        return self.nombre
    def getdireccion(self):
        return self.direccion
    def setNombre(self, pNombre):
        self.nombre = pNombre
    def setDireccion(self, pDireccion):
        self.direccion = pDireccion


def agregar():
    nombre = input("Ingrese el Nombre de la Clinica: ")
    direccion = input("Ingrese la Direcion: ")

    clinicaNueva = Clinica (nombre, direccion)
    listaClinicas.append(clinicaNueva)

def informar():
    print(" ")
    print("----Informe de Clinicas----")
    for indice in range(0, len(listaClinicas)):
        print(f"{indice +1} - {listaClinicas[indice]}")

def borrar():
    informar()
    indice = int(input("Ingrese el numero de cita que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaClinicas[indice -1].getapellido()} {listaClinicas[indice-1].getnombre()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaClinicas.remove(listaClinicas[indice -1])

def modificar():
    informar()
    indice = int(input("Ingrese el numero de Clinica que desea modificar: "))
    nombre = input("Ingrese en nuevo nombre: ")
    listaClinicas[indice - 1].setNombre(nombre)
    direccion = input("Ingrese la nueva Direccion: ")
    listaClinicas[indice - 1].setDireccion(direccion)

listaClinicas = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Clinicas--------")
    print ("A - Agregar Clinica")
    print ("M - Modificar Clinica")
    print ("L - Listar las Clinica")
    print ("B - Borrar Clinica")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'a'):
        agregar()
    elif(opcion == 'l'):
        informar()
    elif(opcion == 'b'):
        borrar()
    elif(opcion == 'm'):
        modificar()
    else :
            print("Opcion Incorrecta")
